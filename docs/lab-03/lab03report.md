# Lab 03: Report

[Back to README](../../README.md)

The following report will include the **activity diagram**, **sequence diagram**, and **state diagram** for our University Module Registration System.

### Table of Contents
Click on the section you want to visit.

| **No.** | **Section**                           | **Contributors**            |
|---------|---------------------------------------|-----------------------------|
| 1       | [Activity Diagram](#activity-diagram) | Xiaosong Hu; Moeka Amatsuji |
| 2       | [Sequence Diagram](#sequence-diagram) | Joseph Thenara; Qianjun Ma  |
| 3       | [State Diagram](#state-diagram)       | Xuan Huang; Jialin Li       |

## Activity diagram
### Diagram Image
![Lab 03 Activity Diagram](images/Lab-03_MI_Activity-diagram.jpg "Activity Diagram")

### Introduction
This graph mainly introduce the flowchart of the designed software. It is divided into four layers with three users and the system itself.

### Description
**Student** is at the top layer. The software shall provide communication between **students** and **student service** to collect and submit form and proof of relevant proficiency if needed.  

The form will be sent to the second layer where the **system** lies, serving as interface between **students** and **Module Convenor** and **the Head of Teaching**. System will check if the module selection is in 60-60 style. It means the credit distribution should be 60 credits for each semester. Otherwise, for example, if the form is in 50-70 style, the form will be sent to **the Head of Teaching** for further approval. **The Head of Teaching** shall check the student record to determine if the student is able to take a 70 credits semester.

Going back to the second layers, furthermore, **the system** will check if the **student** is from the same department as the module required. If the **student** is from another department, the form should be sent to **the Head of Teaching** for approve further and go down to the bottom where **Module Convenor** is. **Module Convenor** will check the class size and if the student has taken pre-requested module to approve it.
Besides all these functions, the software also shall be able to integrated with other software already existed, for example, *Timetabling* to generate timetable and *BlueCastle* to record student performance, to provide service for users.

## Sequence diagram
### Diagram Image
![Lab 03 Sequence Diagram](images/Lab-03_MI_Sequence-diagram.jpg "Sequence Diagram")

### Description
The sequence diagram above illustrates the detailed ***step-by-step*** processes and ***relationship*** between all systems and actors.

#### Main relationships
Referring to the [documentation](../lab-01/lab1analysis.md) of lab 01, the main relationships here would be:
- Students - Student Services
- Student Services - Existing systems
- Staff - Existing systems

Additionally there are also contacts between **students** and **other staff** (i.e. Module Convener and Head of Teaching).

#### Sequence detail
The sequence for students wanting to register for module are divided into three sections:\
(Shown with lines to separate each section and bubbled comments on the diagram above.)


##### 1. Registration
  - Mainly involving **students** and **student services**
  - **Students** choose their preferred modules and submit form which **student services** can validate.
##### 2. Additional requirements.
  - Mainly involving **students** getting approval from **other staff**
  - Submit requirements to **student services**
##### 3. Success and feedback
  - Report generation by **existing software**
  - **Student services** updates information on system
  - **Students** get their enrolment information\
   (e.g. course information/timetable)
  - Relevant **staff** receives relevant report\
  (e.g. student list/course timetable)




## State diagram
### Diagram Image
![Lab 03 State Diagram](images/Lab-03_MI_State-diagram.png "State Diagram")

#### Description
This state diagram describes the whole process students choose his/her modules.

#### Process
##### 1. The **students** first *fill the form* and *submit* it.
##### 2. The **student services** will *check if the form is in compliance with requirements*.
  - The modules should have 120 credits, with a 60-60 credit each semester.
    - If students want a 50-70 credit split, they should get approval from the head of teaching.
  - Students have to take modules from their school and their grades.
    - If students want to take an introductory module from another department, they need the approval from the module convener of the module and the head of teaching of their own school.
##### 3. The choices must be *fed into other university software* like **BlueCastle** and **Timetabling**.
