# User Persona 05 - Staff

#### Amy Lin | Student Services
**Provides assistance for students in trouble**\
![Amy's Photo](img/staff01-amy.jpg)
> Hi! I'm Amy from Student Services. How can I help you today?

##### The facts
- Female
- 28 years-old
- Single
- B.A. Business Administration
- Speaks English and Mandarin Chinese proficiently
- Proficient in using computer for office work
- Extensive user of internet (> 6 hours per day)
- Uses computer and internet mostly on her work desk
  - Checks email every day.
  - Accesses the university various systems on daily basis.
  - Students usually come to her to get help about their various university-related problems (e.g. course enrolment, timetabling, wrong student data, etc.)
- Doesn't access the university systems after her work hours are done (after 5 PM)
- Skilled in doing paperwork and reports

##### Description

Amy has been a student services staff for the past 4 years. Her job entails her to help students that are having problems with the various university-related matters, such as course enrolment, timetabling, student information updates and correcting, etc.

##### Key Attributes
- Highly organized
- Friendly and hospitable
- Skilled in handling people
- Able to process various information given
