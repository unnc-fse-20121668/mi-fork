# User Persona 02 - Student

#### Xiaolan Wu (Michelle) | The Domestic Student
**UG Year 1 - International Business Major**\
![Michelle's Photo](img/student02-michelle.jpg)
> Oh I found this really interesting article from WeChat Moment!

##### The facts
- Female
- 18 years-old
- Currently single, looking for a boyfriend
- Fresh high-school graduate
- Speaks Mandarin Chinese and Cantonese at native level, still trying to improve her English.
- Knows how to use computer for school work (word processing, spreadsheet, powerpoint)
  - But is hopeless when faced with complex computer systems
- Heavy user of internet (> 4 hours per day)
- Usually accesses the web through her smartphone
  - Spends most time in social media (e.g. WeChat & Instagram)
  - Know only basic operation for most system
  - She often confuse herself when filling the forms and ask for help from her friends.
- A good student who prepares herself for lessons ahead of times.
- Almost always submits works well-ahead of the deadline
- Have a good sleeping-habit

##### Description

Michelle is a freshman in the university, starting her preliminary year's CELE programme this year. She just graduated high school last June and is currently having difficulties to register for her course modules. But since she lives in the dorm with her roommates, they can help each other figure out how the registration platform works.

Although she is very academically focused, she also have interest in making friends especially with international students. She actively tries to improve her oral English by befriending foreign students. She spends most of her time checking messages from friends on her social medias (i.e. WeChat).

##### Key Attributes
- Put effort in all tasks
- Large circle of friends
- Well-organized
- Punctual
- Puts too much emphasis on academics
- Diligent
