# User Persona 05 - Staff

#### Phil Norton | Module Convenor
**Module Convener for Economics1**\
![Jake's Photo](img/staff02-phil.jpg)
> Extra reading makes your future brighter

##### The facts
- Male
-  36years-old
- Married, living with wife and two daughters in China together
- Speaks English and French at native level, knows basic Mandarin Chinese   
- Academic Qualification
  - PhD 2010-2016 University of Sheffield
  - MSc International Business Management, University of Liverpool
  - BA Business Management and Economics
- Not good at technologies
  - Tend to ask colleagues about technical issues
  - Prefer phone call more than texting
- Outdoor person
- Loves playing golf with colleagues on weekends


##### Description

Phill is an academic staff at FoB. He came from the United Kingdom and started working in China from 2017. He was interested to work abroad and also in Chinese culture, so he decided to work at UNNC. He loves helping students and reply to students question quick. He loves enthusiastic students like he was in the university.



##### Key Attributes
- Friendly to students
- Careless sometimed
- High communication skill


