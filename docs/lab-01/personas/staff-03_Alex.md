# User Persona 06 - Staff

#### Alex Williams | Head of Teaching
**Organizes the teaching activities in the university**\
![Alex's Photo](img/staff03-alex.jpg)
> I expect only the highest standard of teaching for our student's education!

##### The facts
- Male
- 42 years-old
- Married, 2 child
- M.A. International Higher Education
- Speaks English at native level, knows very little Mandarin Chinese.
- Able to use computer for office work.
- Light user of internet (< 4 hours per day)
- Usually accesses the web through office PC
  - Rarely opens the university system unless there is a need to make reports or rectify errors
  - Extensive user of email
  - Needs help for various IT-related tasks from IT Services
- Doesn't access university platforms after work hours
- Natural-born leader

##### Description

Alex has been delving in the education sector for more than 20 years as of now. He is highly knowledgeable especially in the field of higher education and management. He used to be a high school Business Studies teacher before continuing for a Masters degree where while studying, he also took up an offer as a teaching assistant in the school of education of his university.

Unlike other typical Head of Teaching, he is able to introduce innovative, yet sometimes daring ideas to his department - most of which were highly successful. He is not afraid of failures, instead he embrace every failure as a learning opportunity for himself and his colleagues.

##### Key Attributes
- Highly efficient
- Servant-leadership
- Good people skill
- Acts like a mentor to his staff
- Friendly to students
- Brilliant management skill
- Innovative
