# CW1/Lab 04: Report | Bank of China Financial Manager App

[Back to README](../../README.md)

This report is written  with the purpose of documenting the **requirements document** for the "Bank of China Financial Manager App". The document will detail the requirements given as per the given ***Requirements Brief***.

### Table of Contents
Click on the section you want to visit.

| **No.** | **Section Navigation**                 | **External Links**                         | **Description**                                          |
|---------|----------------------------------------|--------------------------------------------|----------------------------------------------------------|
| 01      | [Project Details](#project-details)    | -                                          | Client's background information; Project description and tasks; Project Brief; |
| 02      | [Requirements](#requirements)          | [View document](lab04requirements-docs.md) | Rationale and Main Document for Project Requirements     |
| 03      | [Others](#others)                      | -                                          | Questions, Clarifications, Notes, and Further Planning   |

## Project Details
### Project Team Information
##### Project Manager
Harvey Spencer

##### Team Members
Xuan Huang; Xiaosong Hu; Jialin Li; Qianjun Ma; Joseph Thenara; Moeka Amatsuji

### Client Information
#### Bank of China
[![Bank of China Logo](images/BOC_Logo.png)](http://www.boc.cn/)

Click on the logo to visit client's website

<details>
  <summary>View full information</summary>

  | **Information**  | **Details**                                      |
  |------------------|--------------------------------------------------|
  | Native Name      | 中国银行股份有限公司                               |
  | Type             | Public                                           |
  | Industry         | Banking; Financial Services                      |
  | Founded          | Beijing, China (1912)                            |
  | Area served      | World-wide (Main services in China Mainland)     |
  | Key People       | Chen Siqing, President and Chairman of the board |

</details>

### Tasks
Design an app for financial management of the clients of Bank of China that retrieves user data directly from the company's database.

### Requirements Brief
<details>
  <summary>Click to view</summary>
  We’ve been approached by a major bank, Bank of China (BoC), for an interesting new app for customers – it’s for personal finance management, using data directly from their accounts, but in a different app. They’ve found out that many clients, especially families or those with several accounts, use spreadsheets to manage their tight budgets. They want to know how much they really spend on different categories, and how much goes to fun casual spending for different things. Equally, they also want to know how much money they have left each month for fun and casual spending, so they don’t accidentally spend money needed for bills. Some clients who are ‘savers’ do this too, but to maximise how much they can move into savings at the end of a month. Some clients that are worried about budgets might not be as worried about categorising everything, but still want to see what’s spent, how much is still going to come out of the account before pay day, and what is free to use.

  Some customers they spoke to (even single-account-single-customers) use spreadsheets to see a total of how much money is exactly fixed every month for bills. They also use these spreadsheets to see which categories are definitely scheduled to go out but are variable (like a phone bill). For these monthly variable payments, they want to set an ‘average’ or ‘usual cost’ for them that’s the same every month and update the spreadsheet with an exact amount when it has gone out of their account. Then, this changes the ‘spare money’ column, positively or negatively. Some clients also create a standard amount that is ‘set aside’ for e.g., food or gifts or for going out, or indeed ‘for savings’, so that their spare money column doesn’t include those amounts. As they spend on these set-aside categories, they subtract it from the set amount for that category. All this helps them calculate an available spare budget, which means, of course they also put in their ‘normal’ income into the spreadsheet, and those have extra income sometimes add it to the month’s income.

  Some customers say they copy every expenditure into a separate sheet, give it a date, and categorise it, so that it comes out of the different categories (like the set aside categories), or just so they can see a total ‘for all money spent on clothes;. I guess in the app, they could categorise a shop the first time it comes up in the app, and then, by default, automatically categorise future spending at the same shop. However, customers pointed out that something might go in a different category (like a clothing item being a gift, rather than going under the clothing category), so they need to be able to change the default category for a transaction from a recognised shop. People’s categories vary from customer to customer, but all have similar types of categories – they all seem to be of type: fixed, variable, set aside, spare (and maybe unknown). So, the rent category might be of type: fixed, and saving and clothing and going-out budgets, might all be of type: set-aside. They like to see totals for categories (e.g., ‘car’). And they like to see totals for types (e.g., ‘set-aside’).

  Multi-account-users might want to also see the above information as single summary that calculated across all accounts, while still seeing summary for each individual account. They might want to check that there is enough money left in one specific account for the plan for that account. But if they don’t have multiple accounts, we don’t keep the app to show confusing and irrelevant functionality. Joint-account users might want to see who has spent money – need to find out more.
</details>

[\[Back to Top\]](#cw1lab-04-report-bank-of-china-financial-manager-app)


## Requirements
**Main Document:** [Requirements Document](lab04requirements-docs.md)

The requirement document contains **textual analysis** of the project brief, along with the **personas** and **user stories** for the identified actors.

### Rationale
This section will describe in detail about the rationale behind using each of the following in the requirement documentation.

#### Textual Analysis
The textual analysis was done to **identify relevant actors and their types**, along with any **possible use-case scenarios**. This were then used as the base for constructing the various personas and user-stories.

#### Personas
The personas were created in order to **represent the target audience** for the app. They were created to represent the various possible user types and their demography.

#### User Stories
The user stories were created based on the personas. The user stories were constructed to identify further what each type of actors will want to do with the app our team is designing. By using the user stories, we can **pinpoint the exact features** that our end-users will want to see in the app.

#### Use-Case Diagram
The use-case diagram **will not be used** in the current document due to several reasons.
- Although there is limited number of possible actors, each actors sub-types are too diverse to be visualized in a use-case diagram.
  - ***e.g.*** - a **student** might have **joint account** with their parents or **single account** depending on their preference.
  - representing this in a use-case diagram will result complex relationship not suitable to be displayed in the diagram.
- The brief provided by our client contains a **large number** of possible use-case each actors can perform.
- The use-case present in the brief has **many overlaps** amongst the various actors
  - Attempting to create the diagram will result in a messy diagram.
  - Too many use-case and actors will result in an unclear diagram due to too many lines present.
- Using the combination of **persona** and **user stories** instead of use-case diagram will be able to **clearly represent** the actors and their possible use-case scenarios.

However in future documents, a use-case diagram may be developed after a more generalised model has been developed or more information has been provided by the client.\
(e.g. more types of actors like bank staff and their use-case)

[\[Back to Top\]](#cw1lab-04-report-bank-of-china-financial-manager-app)

## Others
### Questions, Clarifications, Notes, and Further Planning
1. **Linking the app to other pre-existing systems**
  - Will this be a standalone app?
  - Linked to other existing app/systems?
2. **Report generation functionality**
  - This is a financial manager app - do we want to add financial report generation functionality?
  - Other reports? e.g. Joint-Account: Each user's financial report
3. **Bank Staff Access**
  - Will there be staff-only edition for the app? (i.e. for admin purposes)
  - What can BOC staff do that user's cannot do?
4. **Specific requests from client**
  - Any specific things to include?
  - The brief only mentions about what the client's customers want.
5. **Access to database/sample database for testing**
  - We will need access to the client's database or be provided with testing data that have the same formats with the actual database
  - Since the client wants this app to be able to retrieve data from their database.
6. **Everything here is still in drafting stage**
  - Present and get feedback from the client.
  - Agree on the requirements - get it on paper
  - Note all changes that the client requested
  - If nothing else - confirm deadline and budget or schedule further meetings to discuss

[\[Back to Top\]](#cw1lab-04-report-bank-of-china-financial-manager-app)
