# Requirements Document | BOC App Project
Quick Navigation :
[README](../../README.md) | [Project Report](lab04report.md)


## Table of Contents
Click on the section you wish to visit.

| **No.** | **Section**                            | **Contributors**             |
|---------|----------------------------------------|------------------------------|
| 01      | [Textual Analysis](#textual-analysis)  | Joseph Thenara & Jialin Li   |
| 02      | [Personas](#personas)                  | Xuan Huang & Qianjun Ma      |
| 03      | [User Stories](#user-stories)          | Moeka Amatsuji & Xiaosong Hu |


## Textual analysis
The text below was taken from *Requirement Brief* provided by our client.

##### Analysed Text:
![Textual Analysis](images/textanalysis-01.png)

Color-coding was used to identify the various elements given in the requirement brief. Below are the descriptions of each colours and their groupings.

##### Actors
  - **Yellow:** Customer (actor category)
  - **Orange:** Actor types - Single/Multi/Joint Account (additionally family)

##### Use-case
  - **Green:** Main features that are available to all actors.
  - **Purple:** Feature exclusive to *multi-account user*
  - **Blue:** Feature exclusive to *joint-account users*
  - **Pink:** Feature that may possibly be exclusive for *family* accounts.

##### Extracted Text:
![Textual Analysis Extract](images/textanalysis-02.png)

We have identified only one category for the actor - **customers** - given in the client's project brief. Analysing further, the following actors were then identified.

##### Actors
- **Single-account user**
  + Have access to all main features.
- **Multi-accounts user**
  + Have access to all main features.
  + Additionally have access to multi-accounts features only.
- **Joint-account users**
  + Have access to all main features.
  + Additionally have access to joint-account features only.
- **Family account holder**
  + Additional actor type - can be categorised into one of the three main actors above.
  + Have access to all main features.
  + Additionally have access to family-oriented features only.

##### Use-Case
Additionally, each use-case identified are also described below.

###### Main Features
  + **View Account Information**
    - Account balance
    - Set income and additional incomes
  + **View Spending Detail and Pattern**
    - Summarizes the spending each month
  + **Set Budget**
    - **Set amount** for different budget category (i.e. bills, foods, entertainment, etc.)
    - **See outstanding amount** for each budget
    - **Automatic update** of budget after each spending - both scheduled and not
    - For "***bills***"
      + **Set monthly budget** for bills
      + **Automatically reduce** the total disposable amount of the account by the amount for "bills"
    - For "***set-aside***" budget (e.g. food/gifts/fun/savings)
      + **Not included/shown in "spare budget"**
    - For ***variable spending*** (e.g. mobile fees)
      + Set **average/usual costs**
      + Set **schedule for spending** and **update the account** after it happens
  + **Set Categories for Spending**
    - Select **default categories** after first spending in a specific store (e.g. H&M - 'Clothing' category)
    - Able to **change categories** when needed (e.g. H&M clothing items as 'Gifts')

###### User-Specific Features
  + **Family-budget Planning** (Family Account)
  + **View users who did a specific account activity** (Joint-Account)
  + **View account-specific details** (Multi-Accounts)

[\[Back to Top\]](#requirements-document-boc-app-project)

## Personas
The following personas has been created to represent the various personalities that the end-user may be.\
Click on the names or photos to see their profiles.
<table>
  <tr>
    <th colspan="3"><b>Personas</b></th>
  <tr>
    <td align="center"><b>Single-Account</b></td>
    <td align="center"><b>Multi-Account</b></td>
    <td align="center"><b>Joint-Account</b></td>
  </tr>
  <tr>
    <td align="center">
      <a href="personas/actor01_singleaccount.md">
        <img src="personas/img/actor01-jane.jpg" alt="Jane's Picture">
      </a>
    </td>
    <td align="center">
      <a href="personas/actor02_multiaccount.md">
        <img src="personas/img/actor02-eric.jpg" alt="Jane's Picture">
      </a>
    </td>
    <td align="center">
      <a href="personas/actor03_jointaccount.md">
        <img src="personas/img/actor03-john.jpg" alt="Jane's Picture">
      </a>
    </td>
  </tr>
  <tr>
    <td align="center">[Jane Liu Jingyan](personas/actor01_singleaccount.md)</td>
    <td align="center">[Eric Jonas White](personas/actor02_multiaccount.md)</td>
    <td align="center">[John Warwick](personas/actor03_jointaccount.md)</td>
  </tr>
</table>

[\[Back to Top\]](#requirements-document-boc-app-project)

## User Stories
The table below includes all user stories in detail, categorised by their personas.

<table>
  <tr>
    <th><b>AS A/AN...</b></td>
    <th><b>I WANT TO / WE'D LIKE TO...</b></td>
    <th><b>SO THAT...</b></th>
  </tr>
<!-- First Section -->
  <tr>
    <td colspan="3" align="center">
      <b><i>Single-Account users</i></b>
    </td>
  </tr>
  <tr>
    <td>Company Employee</td>
    <td>see how much money I can save</td>
    <td>I can plan my finance strategy</td>
  </tr>
  <tr>
    <td>Entrepreneur</td>
    <td>divide my spare money into two categories (social/fun and daily spending)</td>
    <td>I can manage my money and investments</td>
  </tr>
  <tr>
    <td>Student</td>
    <td>see the estimated future spending by categories</td>
    <td>I can plan which spending to reduce and which to increase</td>
  </tr>
<!-- Second Section -->
  <tr>
    <td colspan="3" align="center">
      <b><i>Multi-Account users</i></b>
    </td>
  </tr>
  <tr>
    <td>Father (Family Head)</td>
    <td>be able to manage all of our family account - savings, spending, investment, etc. - and see all of our family's spending in different categories</td>
    <td>we can plan our family finance and not spend money meant for bills on casual fun</td>
  </tr>
  <tr>
    <td>Housewife</td>
    <td>an app that can help plan our family finance strategy since there are a lot of bills to consider in addition to other spending</td>
    <td>we can better manage our savings account, on top of our regular account</td>
  </tr>
  <tr>
    <td>Freelance worker</td>
    <td>check the funds by different accounts rather than viewing the integrated amount, since I have two accounts - personal and business</td>
    <td>I can prevent forgetting my income to my personal account</td>
  </tr>
<!-- Third Section -->
<tr>
  <td colspan="3" align="center">
    <b><i>Joint-Account users</i></b>
  </td>
</tr>
<tr>
  <td>Student</td>
  <td>my parents and me to be able to manage my account easily, since my tuition fee is being covered by both scholarship and my parents, in addition to me getting additional income from private tutoring (multiple income for the account)</td>
  <td>I can create a clear financial plan which my parents should also have access to</td>
</tr>
<tr>
  <td>Newly-married couple</td>
  <td>be able to manage our financial plan by cutting unnecessary and minimiseable spending</td>
  <td>we can maximise our saving each month to ultimately afford a house</td>
</tr>
<tr>
  <td>Young couple</td>
  <td>share and review each other's financial activities, since we're going to have a baby next year and make sure there's no wasteful or unneeded spending</td>
  <td>we can start to save certain amount together to support our family</td>
</tr>
</table>

[\[Back to Top\]](#requirements-document-boc-app-project)

Quick Navigation :
[README](../../README.md) | [Project Report](lab04report.md)
