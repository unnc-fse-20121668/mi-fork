# User Persona 02 - Multiple-Accounts User

#### Eric Jonas White | The Alibaba Employee
**General Staff - Alibaba company** | a father who manages his family's various bank accounts

![Eric's Photo](img/actor02-eric.jpg)
> For my family, I'd do anything.

##### The facts
- Male
- 30 years-old
- Married, 1 child
- Employed
- Have the habit of saving money
- Sometimes go online shopping
- English as native level
- Loves to drink with his work buddies
- Lives with his wife and child
- Use internet 3 hours per day
- Have bills to pay per month
- In charge of the money of family
  - Not very tight on budget

##### Description

Eric is a general staff who works in Alibaba. He is a father of one and is in charge for managing his family's multiple financial accounts in Bank of China. He is also responsible for setting his family's budget and separating the various funds for their respective uses - bills, food, fun, etc.

He wishes that there is an app which can update his family budgets in time to know how much his family spend on different categories and how much they can use for entertainment activities so that they will not spend the part for paying bills for entertainment activities.


##### Key Attributes

- Fast-learner
- Hard working
- Prioritise Savings
- Studious
- Multiple Incomes
