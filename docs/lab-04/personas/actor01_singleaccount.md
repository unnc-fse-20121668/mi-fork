# User Persona 01 - Single-Account User

#### Jane Liu Jingyan | The Entrepreneur
**Founder -- The Exciting Company** | a single-account holder who's proficient in financial management

![Jane's Photo](img/actor01-jane.jpg)
> I will finish it!

##### The facts
- Female
- 29 years-old
- Married, 1 child
- Speaks Chinese Mandarin at native level, English at second-language level.
- Lives with her husband and child
- Founder of her company
- Very proficient in managing her and the company's budget
  - Keeps good track of her budget
  - Has a somewhat lax budget due to good income from the company
- Above average computer skill
  - Heavy user of word processing and spreadsheet software
  - Able to use spreadsheets to manage her budget
- Moderate user of internet (~ 4 hours per day)

##### Description

Jane is a new entrepreneur who has established her own company - The Exciting Company. Her company mainly deals in technology-related business activities. She has a fairly stable and solid income from her company, thus her budget is not worrying. Still, she keeps really good track of her funds so that no spending or earning are ambiguous.

Since she manages her own company, she is highly efficient in using spreadsheets to manage her budgets. However, she would love if the bank could provide a more efficient method of financial management so that she can focus her time more on her business and family.

##### Key Attributes
- Passionate
- Focused
- Save money
- Good at financial management
- Always strives to improve
- A bit short-tempered
