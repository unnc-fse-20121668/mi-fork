# CW3/Lab07-09: Report | BOC App Development, Unit Testing, & Integration Testing

[Back to README](../../README.md) | [Requirements](../lab-04/lab04report.md) | [Specifications](../lab-05/lab05report.md)

This report was written with the purpose of documenting the development of the BOC Financial Manager App, focusing on the unit and integration testings. 



## Contents

| #    | Content                                   | Description / Link                                           |
| ---- | ----------------------------------------- | ------------------------------------------------------------ |
| 1    | [Introduction](#1-introduction)           | Introduction to the project and objectives of this project.  |
| 2    | [Approach](#2-approach)                   | Technical information for this project                       |
| 3    | [Team Management](#3-team-management)     | Details of work division, timeline, meeting record, and milestones |
| 4    | [Issues Management](#4-issues-management) | Details on how we deal with issues                           |
| 5    | [Test Plans](#5-test-plans)               | Brief description. View the main document [here](testplan.md). |



## 1. Introduction

### 1.1 Objective

To review the source code provided and design a system of test for the whole system. These tests should test the units of the systems (Unit testing) and also the inter-components cohesiveness (integration testing).

Three java classes have been provided, namely:

+ BoCApp - Main class
+ BoCCategory - Sub-system that handles category operations
+ BoCTransaction - Sub-system that handles transaction operations



The tests should be done following the below requirements:

- Using JUnit Testing for all tests
- Follow appropriate development principles, e.g.
  - The use of comments for tracking purpose
  - Paired coding
  - Use of version control



### 1.2 Scope of Testing

#### 1.2.1 Unit Tests

Unit tests should be done for both **category** and **transaction** sub-systems, and should tests for all methods under each sub-system.

#### 1.2.2 Integration Test

The system integration test of the BoC Financial Manager App will include **category** and **transaction** sub-systems. 

The integration test will not include other planned features not provided by the source code.

### 1.3 System Overview

The BoC Financial Manager App is an app designed to run on mobile devices such as smartphones. The current version of the app will include features that allows end-user to manage their transactions, including categorising function, review budget and remainder, automatic update of each category after transaction is done.

The current version of the system runs independently without any connection to BoC's database.

### 1.4 Definitions

| Item                | Definition                                                   |
| ------------------- | ------------------------------------------------------------ |
| Main System         | The BoCApp class, which is the gateway to the app            |
| Sub-Systems         | Both the BoCCategory and BoCTransaction Classes, the units of the system. |
| Requirements        | Something that the system should do or be.                   |
| Unit Testing        | A level of testing to test each units of the system.         |
| Integration Testing | A level of testing that validates both internal and external components of the system and ensuring that the systems works cohesively as a whole. |





## 2. Approach

### 2.1 Assumptions / Constraints

#### 2.1.1 Assumptions

+ The first build of the BOC Financial Manager App will be ready for system integration testing on 06 May 2020.
+ Each build of the app will have passed unit testing before integration testing are done.

#### 2.1.2 Constraints

+ Three-weeks timeframe may not be enough to develop a very comprehensive system of tests.
+ The provided source code is of low-quality, more efforts will need to be given for code refactoring.

### 2.2 Coverage

Test coverage will be measured by :

+ A completed matrix of testable requirements and test cases (view in test plan document).

In the event where coverage levels are not met, the Quality Assurance Manager will determine whether the actual result are adequate when referring to the system risks.

### 2.3. Test Environment

1. Asus
   + Intel Core i7-7700
   + 16GB DDR3 RAM
2. Alienware
   + Intel Core i7 - 8700
   + 16GB DDR3 RAM
3. Apple MacBook Pro
   + Intel Core i5 - 3210M
   + 8GB DDR4 RAM



### 2.4 Test Types

+ **Unit Testings** for sub-systems using **JUnit Tests**
+ **Integration Testing** for main system using **JUnit Tests**



## 3. Team Management

### 3.1 Resources

#### 3.1.1 Roles

| Role                      | Name(s)                                                      | Specific Responsibilities / Comments                         |
| ------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Project Manager           | Xiaosong Hu                                                  | Organising meetings<br />Work division<br />                 |
| Transaction Class Team    | Xiaosong Hu (Lead)<br />Xuan Huang (Member)<br />Moeka Amatsuji (Member) | Creating unit test for BoCTransaction class<br />Review and modify source code<br />Creating test plan<br />Commenting |
| Category Class Team       | Qianjun Ma (Lead)<br />Jialin Li (Member)<br />Joseph Thenara (Member) | Creating unit test for BoCCategory class<br />Review and modify source code<br />Creating test plan<br />Commenting |
| Integration Team          | Moeka Amatsuji (Lead)<br />Joseph Thenara (Member)           | Creating integration test for the app<br />Review and modify source code<br />Creating test plan<br />Commenting |
| Report Manager            | Joseph Thenara                                               | Producing final report<br />Standardise report format<br />Standardise comments<br />Combine all documents under one report |
| Initial Coder             | Louis Litt                                                   | Provides source code                                         |
| Quality Assurance Manager | Jessica Pearson                                              | Ensuring quality standard for all deliverables               |




### 3.2 Meetings

<table>
  	<tr align="center">
    	<th align="center"><b>No.</b></th>
    	<th align="center"><b>Date</b></th>
    	<th align="center"><b>Content</b></th>
    	<th align="center"><b>Attendance</b></th>
    	<th align="center"><b>Notes</b></th>
  	</tr>
  <!--First Meeting-->
  	<tr>
    	<td align="center">1</td>
    	<td>23 April 2020 (Thu)</td>
    	<td>
      		Initial Planning<br>
      		Work Distribution
    	</td>
        <td align="center">All</td>
        <td>Online</td>
	</tr>
  	<tr>
    	<td align="center">2</td>
    	<td>30 April 2020 (Thu)</td>
    	<td>
            Planning for unit testing<br>
            Planning for integration testing
    	</td>
        <td align="center">All</td>
        <td>Online: Moeka</td>
  	</tr>
  	<tr>
    	<td align="center">3</td>
    	<td>01 May 2020 (Fri)</td>
    	<td>
        	Building unit tests using JUnit for the classes: <br>
        	<b>BoCTransaction</b> and <b>BoCCategory</b>
    	</td>
        <td align="center">All</td>
        <td>Online: Moeka</td>
	</tr>
	<tr>
        <td align="center">4</td>
        <td>02 May 2020 (Sat)</td>
        <td>
        	Continuing unit testing for both sub-systems<br>
            Preparation for integration testing
        </td>
        <td align="center">All</td>
        <td>Online: Moeka</td>
	</tr>
	<tr>
        <td align="center">5</td>
        <td>07 May 2020 (Thu)</td>
        <td>Finalising report and concluding coursework.</td>
        <td align="center">All</td>
        <td>Online: Moeka</td>
	</tr>
</table>



### 3.3 Timeline

| No.  | Date          | Description                             | To-Do / Comments                                             | Expected Due Date |
| ---- | ------------- | --------------------------------------- | ------------------------------------------------------------ | ----------------- |
| 1    | 30 April 2020 | All test plans are done.                | Update test plan on GitLab.                                  | 07 May 2020       |
| 2    | 01 May 2020   | Unit testing for sub-systems started    | Finish unit testing for both sub-systems                     | 03 May 2020       |
| 3    | 03 May 2020   | Unit tests done                         | -                                                            | -                 |
| 4    | 03 May 2020   | Code fixing start                       | Code fixing for all classes. Some codes have been fixed during unit testing. | 07 May 2020       |
| 5    | 04 May 2020   | Integration testing started             | Finish integration testing for main class. <br /> (Note: to test how each sub-systems work with each other) | 05 May 2020       |
| 6    | 05 May 2020   | Massive overhaul on integration testing | Wrong approach was taken for integration testing. **Overhauled**, expected due date delayed by one day. | 06 May 2020       |
| 7    | 06 May 2020   | Integration Testing Done                | -                                                            | -                 |
| 8    | 07 May 2020   | Report finalisation                     | Finalise report<br />Complete all test plans<br />Update all files<br />Complete all merge | 07 May 2020       |

### 3.4 Git

#### 3.4.1 Branching

The project utilises the branching feature of git to assist in keeping track of different tasks.

+ Master Branch
  + Main branch that contains original source code
+ Testing Branch
  + Main development branch where tests were written and codes are fixed.
  + All tests are done in this branch. When all of the current tests and codes are verified, this branch will be merged with the **master** branch
+ Report Branch
  + Specific for report writing purposes.
  + Branched from testing to allow for latest codes.

### 3.5 Milestones

| Milestone # | Description                                                 | Date Achieved |
| ----------- | ----------------------------------------------------------- | ------------- |
| 1           | Complete reviewing the provided source code                 | 23/04/2020    |
| 2           | Test plans for Category and Transaction sub-systems created | 30/04/2020    |
| 3           | Tests for both sub-systems started development              | 01/05/2020    |
| 4           | Completed development of unit tests for both sub-systems    | 03/05/2020    |
| 5           | Created test plan for system integration testing            | 03/05/2020    |
| 6           | Integration test development started                        | 04/05/2020    |
| 7           | Completed development of Integration test                   | 06/05/2020    |
| 8           | Completed refactoring of source code                        | 07/05/2020    |
| 9           | Merging all branch into **master**                          | 07/05/2020    |
| 10          | Completed report                                            | 07/05/2020    |



## 4. Issues Management

The following were done to keep track of any issues encountered during our work:

###  A. Discussion regarding changing codes

Where an issue is encountered, the team responsible for the codes will notify their members and communicate on how to resolve the issue. 

### B. Commenting

To identify issues, comments shall be used not only for identifying, but also for proposing solutions and pointing out further possible issues.



## 5. Test Plans

### 5.1 Description

+ The test cases designed for unit testing will cover the requirements comprehensively. The test data will be categorised either normal or abnormal as needed, where normal are the expected inputs from users, and abnormal data are erroneous inputs.
+ The test cases designed for integration testings will be adapting the **use cases** described in the [specifications document](../lab04requirements-docs.md#user-stories). 
  + Due to this, the integration test will only test for valid cases. 
  + This is because all erroneous cases should already be tested and handled during unit testing.

### 5.2 Main Test Plan Document

[Jump to test plan](testplan.md)



## 6. Source Code

| Classes                                                      | Tests                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [BoCApp Main Class](../../src/cw3-java-src/src/BoCApp.java)  | [Integration test](../../src/cw3-java-src/tests/BoCAppTest.java) |
| [BoCTransaction Sub-System](../../src/cw3-java-src/src/BoCTransaction.java) | [Transaction Unit Test](../../src/cw3-java-src/tests/BoCTransactionTest.java) |
| [BoCCategory Sub-System](../../src/cw3-java-src/src/BoCCategory.java) | [Category Unit Test](../../src/cw3-java-src/tests/BoCCategoryTest.java) |

### Class Documentation

[Documentation HTML](../../src/cw3-java-src/docs/index.html)

The above link is a javadoc document that documents all classes of the app, including their methods and each the inputs and return values. If you are not able to view it, it is recommended to download the ***docs*** folder and run it through your browser.