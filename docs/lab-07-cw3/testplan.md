# CW3/Lab07-09: Test Plan

[Back to README](../../README.md) | [Main Report](cw3-report.md) | [Requirements](../lab-04/lab04report.md) | [Specifications](../lab-05/lab05report.md)


| **No.** | **Navigation** |   **Description**   |
|---------|----------------|---------------------|
|    1    | [Unit Tests](#test-plans-unit-testing)<br />( [BoCCategory](#boccategory) - [BoCTransaction](#boctransaction)  ) | Details of unit tests |
| 2 | [Integration Test](#integration-test) | Details of Integration test |



## Test Plans: Unit Testing
**You should add rows and even columns, and indeed more tables, freely as you think will improve the report.**

### BoCCategory:

<table>
        <tr align="center">
            <td><b>Test</b></td>
            <td><b>Inputs</b></td>
            <td><b>Expected Outcome</b></td>
            <td><b>Test Outcome</b></td>
            <td><b>Type</b></td>
            <td><b>Description</b></td>
            <td><b>Dates</b></td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">Default Constructor</th>
        </tr>
        <tr>
            <td>testDefaultConstructor()</td>
            <td><i>Left empty</i></Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Creating new category without parameter</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">Main Constructor</th>
        </tr>
        <tr>
            <td rowspan="4">testMainConstructors()</td>
            <td>"Test"</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Creating new category "Test"</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>"3320 Category"</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Creating new category alphanumeric name "3320 Category"</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>"!@#$%^&*()_-+="</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Creating new category name with special characters "!@#$%^&*()_-+="</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>null</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Creating new null category (will use default constructor instead of main)</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">CategoryName</th>
        </tr>
        <tr>
            <td rowspan="4">testGetCategoryName()</td>
            <td>"Test"</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Create category "Test" then fetch it</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>"!@#$%^&*()_-+=3320 Category"</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Create category with a mix of alphanumeric and special characters as the name, then fetch it</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>""</Td>
            <td>FAIL</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Create category with blank name then fetch it</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td><i>null</i></Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Create null category, then attempt to fetch. Test should use default constructor instead and created a default category.</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">CategoryBudget</th>
        </tr>
        <tr>
            <td rowspan="1">testGetCategoryBudget()</td>
            <td>Default value</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Create new category with default budget value.</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">CategorySpend</th>
        </tr>
        <tr>
            <td rowspan="1">testGetCategorySpend()</td>
            <td>Default value</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Create new category with default spending value.</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">setCategoryName</th>
        </tr>
        <tr>
            <td rowspan="2">testSetCategoryName()</td>
            <td>"Alternate"</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Sets current category name to "Alternate".</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>"" (empty string)</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Sets current category name to "".</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">setCategoryBudget</th>
        </tr>
        <tr>
            <td rowspan="2">testSetCategoryBudget()</td>
            <td>10.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Sets current category budget to 10.00</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>-100.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>
                Sets current category budget to -100.00. <br />
                Value should not update since negative value is not accepted.
            </td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">addExpense</th>
        </tr>
        <tr>
            <td rowspan="2">testAddExpense()</td>
            <td>-100.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Adds a spending expense of value -100.00, value should not change because negative value is invalid.</td>
            <td>02 May 2020</td>
        </tr>
        <tr>
            <td>11.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Adds a spending expense of value 11.00</td>
<td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">removeExpense</th>
        </tr>
        <tr>
            <td rowspan="2">testRemoveExpense() (initialised with 10.00)</td>
            <td>1.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Remove expense of normal value.</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>-100.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Abnormal</td>
            <td>Remove expense of a negative value, value should not change because negative value is invalid.</td>
            <td>02 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">resetBudgetSpend</th>
        </tr>
        <tr>
            <td rowspan="1">testResetSpendTotal()</td>
            <td>N/A</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Resets the expensed value to zero.</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">getRemainingBudget</th>
        </tr>
        <tr>
            <td rowspan="2">
                testCalculateRemainingBudget()<br>
                (Initialised budget with 10.00)
            </td>
            <td>1.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Calculates remaining budget after expense added. (10.00 - 1.00 = 9.00)</td>
            <td>01 May 2020</td>
        </tr>
        <tr>
            <td>10.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Calculates remaining budget after expense added. (9.00 - 10.00 = -1.00)</td>
            <td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">toString</th>
        </tr>
        <tr>
            <td rowspan="1">testToString()</td>
            <td>Default</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>Check if the return of the class is overridden. <br>
                Return format should be: "name (budget) - remaining (status)"
            </td>
<td>01 May 2020</td>
        </tr>
        <!---------------------------------------------->
        <tr align="center">
            <th colspan="7">calcRemainingStatus</th>
        </tr>
        <tr>
            <td rowspan="2">
                testCalcRemainingStatus()<br>
                (Initialised budget with 1000.00)
            </td>
            <td>322.81</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>When budget &lt spending, should result in 'remaining'.</td>
            <td>06 May 2020</td>
        </tr>
        <tr>
            <td>1200.00</Td>
            <td>PASS</td>
            <td>PASS</td>
            <td>Normal</td>
            <td>When budget &gt spending, should result in 'overspent'.</td>
			<td>06 May 2020</td>
        </tr>
    </table>





### BoCTransaction:

<table>
    <tr align="center">
        <td><b>Test</b></td>
        <td><b>Inputs</b></td>
        <td><b>Expected Outcome</b></td>
        <td><b>Test Outcome</b></td>
        <td><b>Type</b></td>
        <td><b>Description</b></td>
        <td><b>Date</b></td>
    </tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">transactionName</th>
    </tr>
	<tr>
        <td>testTransactionName()</td>
        <td>"Phone Bill"</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Create transaction with name "Phone Bill".</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">transactionValue</th>
    </tr>
	<tr>
        <td>testTransactionValue()</td>
        <td>37.99</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Create transaction with value 37.99.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">transactionCategory</th>
    </tr>
	<tr>
        <td>testTransactionCategory()</td>
        <td>1</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Creating new transaction with category index 1.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">transactionTime</th>
    </tr>
	<tr>
        <td>testTransactionTime()</td>
        <td>Time of creating transaction<br>
			( currentDate.tostring() )
		</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Assigns creation time to the created transaction.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">setTransactionName</th>
    </tr>
	<tr>
        <td>testSetTransactionName1()</td>
        <td>null</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Abnormal</td>
    	<td>Try to catch exception when trying to set transaction name into <i>null</i> and checks if name is set into null or not.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionName2()</td>
        <td>"testName1"</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets transaction name into "testName1"</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionName3() [obsolete]</td>
        <td>"testName1"</Td>
    	<td>PASS</td>
    	<td>FAIL</td>
		<td>Normal</td>
    	<td>Sets transaction into existing name and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionName3new()</td>
        <td>"testName1"</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets transaction into existing name and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">setTransactionValue</th>
    </tr>
	<tr>
        <td>testSetTransactionValue0() [obsolete]</td>
        <td>0.00</Td>
    	<td>PASS</td>
    	<td>FAIL</td>
		<td>Normal</td>
    	<td>Sets transaction value with lower value and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionValue0new()</td>
        <td>0.00</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets transaction value with lower value and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionValue1()</td>
        <td>37.99</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets transaction value into 37.99.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionValue2() [obsolete]</td>
        <td>37.99</Td>
    	<td>PASS</td>
    	<td>FAIL</td>
		<td>Normal</td>
    	<td>Sets value to an unavailable value and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTransactionValue2new()</td>
        <td>37.99</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets value to an unavailable value and catch the exception.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">setTransactionCategory</th>
    </tr>
	<tr>
        <td rowspan="2">testSetTransactionCategory()</td>
        <td>10</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets category index to 10.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>0</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Abnormal</td>
    	<td>Sets category index to 0, should not change because zero is not an acceptable value.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">setTransactionTime</th>
    </tr>
	<tr>
        <td>testSetTransactionTime()</td>
        <td>"2000-11-21 21:46:11"</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Sets the time of transaction to 2000-11-21 21:46:11.</td>
    	<td>02 May 2020</td> 
	</tr>
	<tr>
        <td>testSetTimeWithNull()</td>
        <td>null</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Abnormal</td>
    	<td>Sets time to null, time value should not change because null is not acceptable.</td>
    	<td>02 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">toString</th>
    </tr>
	<tr>
        <td>testToString() [obsolete]</td>
        <td>"Phone Bill - ¥37.99"</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Checking whether toString works as intended.<br> 
            (Note: this test did not test the toString() method from BoCTransaction class.)</td>
    	<td>03 May 2020</td> 
	</tr>
	<tr>
        <td>testToStringnew()</td>
        <td>(Transaction date) + " | Internet Bills - ¥119.99"</td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Checking whether toString works as intended.<br>
        	i.e. returns a formatted string of transaction record.
        </td>
    	<td>03 May 2020</td> 
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">isComplete</th>
    </tr>
	<tr>
        <td>testIsCompleter()</td>
        <td>Default, "Mobile Phone Bill", 332.00</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Normal</td>
    	<td>Checks whether isComplete succesfully detects if a transaction name and value already exists and whether they are available to be used.</td>
    	<td>03 May 2020</td> 
	</tr>
</table>



## Integration Test

### BoCApp:

<table>
    <tr align="center">
        <td><b>Testing Method</b></td>
        <td><b>Inputs</b></td>
        <td><b>Expected Outcome</b></td>
        <td><b>Test Outcome</b></td>
        <td><b>Purpose</b></td>
        <td><b>Description</b></td>
        <td><b>Date</b></td>
    </tr>
<!---------------------------------------------->
<tr align="center">
  <th colspan="7">testUseCase1()</th>
</tr>
<tr>
    <td>CategoryOverview()</td>
    <td>none</Td>
  <td>PASS</td>
  <td>PASS</td>
<td>Checking if the category overview displays all categories with their title, budget and spent correctly.</td>
  <td>This use case is based on the first user story. The use case is that this user sees the category overview to achieve his aim.<br><a href="docs/lab-04/lab04requirements-docs.md">User stories</a></td>
    <td>06 May 2020</td>
</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">testUseCase2()</th>
    </tr>
	<tr>
        <td>AddCategory()</td>
        <td>"Daily spending", 750.00</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Checking if user input "Daily spending" category has been created and added to "UserCategories" list with correct title and budget.</td>
    	<td>Based on the second user story. Assume that this user adds "Daily spending" category to achieve his aim.<br><a href="docs/lab-04/lab04requirements-docs.md">User stories</td>
    <td>06 May 2020</td>
	</tr>
<!---------------------------------------------->
    <tr align="center">
    	<th colspan="7">testUseCase3()</th>
    </tr>
	<tr>
        <td>ListTransactionsForCategory()</td>
        <td>4</Td>
    	<td>PASS</td>
    	<td>PASS</td>
		<td>Checking if all transaction in the user chosen category("Groceries") are displayed.</td>
    	<td>Based on the third user story. The use case is that the user checks transaction by category to achieve his aim.<br><a href="docs/lab-04/lab04requirements-docs.md">User stories</td>
    <td>06 May 2020</td>
	</tr>
<!---------------------------------------------->
<tr align="center">
  <th colspan="7">testUseCase4()</th>
</tr>
<tr>
    <td>AddCategory()</td>
    <td>"Rent", 2600.00</Td>
  <td>PASS</td>
  <td>PASS</td>
<td>Checking if newly created "Rent" category has been added to "UserCategories" list.</td>
  <td rowspan="4">Based on a user story: "As an owner of a family-run company, I want to see integrated amount of my personal account with business account. So that I can see the total spent". 
  Assume that this user newly rented a office, and he want see the total amount on house and office rent.  Use case is that firstly, the user creates a category for rent, 
  then move the transaction of house rent and add office rent to it to achieve his aim.</td>
    <td>06 May 2020</td>
</tr>
<tr>
    <td>ChangeTransactionCategory()</td>
    <td>0, 4</Td>
  <td>PASS</td>
  <td>PASS</td>
<td>Checking if the "Rent" transaction's category has been changed from "Unknown" to "Rent". And checks if both categories' spent has been updated.</td>
    <td>06 May 2020</td>
</tr>
<tr>
    <td>AddTransaction()</td>
    <td>"Office rent", 1250.99</Td>
  <td>PASS</td>
  <td>PASS</td>
<td>Checking if the newly created transaction "Office rent" has been added to the "UserTransactions" list with correct title and value.</td>
    <td>06 May 2020</td>
</tr>
</table>

