# CW2/Lab 05: Report | Specifications Document

[Back to README](../../README.md) | [Review Requirements](../lab-04/lab04report.md)



### Table of Contents
Click on the section you want to visit.

| **No.** | **Section Navigation**                 | **Description**                                                                              | **Contributors**                  |
|---------|----------------------------------------|----------------------------------------------------------------------------------------------|-----------------------------------|
| 01      | [Rationale](#rationale)                | Reasoning for selecting the items in this report.                                            | N/A                               |
| 02      | [Prototypes](#prototypes)              | App prototypes and descriptions, including interface design and functionalities description. | Xiaosong Hu; Joseph Thenara       |
| 03      | [Context Diagram](#context-diagram)    | A diagram that shows relationship between the parts of the system and their functions.       | Moeka Amatsuji; Joseph Thenara    |
| 04      | [Scenarios](#scenarios)                | Describes how users can achieve a specific goal in a structured, step-by-step manner.        | Jialin Li; Qianjun Ma; Xuan Huang |

## Rationale
**Prototypes**\
These have been created to **demonstrate and illustrate** how the app should look like and how it will work. Since our client is not tech-savvy, we aim to create an easy-to-understand prototypes. However as the name "*prototype*" suggests, the current document is not final and **will** change in the future. Currently we are building these prototypes to **see how the requirements and specifications can integrate** as a "*finished product*".

**Context Diagram**\
This was chosen since our project mainly **focuses on the interaction between actors and the app itself**. Additionally, a context diagram is able to **capture the various relationships and interactions between parts of the system**, thus able to provide a clear view of how the parts work with each other in regards to working as a whole.

Also by creating a context diagram, we are able to **identify which parts of the system should be prioritised** and **which should not be developed**. The context diagram **establishes a boundary** of what our app should be able to do and what it shouldn't do.

**Scenario**\
Scenarios provide us with a **breakdown** of what goals do our end users wants to achieve using this app. By providing a specific scenario for each goal, we can **identify possible challenges** and possibly **point of errors**. We can list a **step-by-step** scenario of what each user tries to achieve and **identify the steps** needed to achieve that goal. By identifying these steps, we can then focus on the functions that are important, thus helping the team to prioritise the parts of the system.

In addition, by **identifying possible point of errors** the team as a developer can see what things can go wrong when users are using the app. Thus, we can implement **fail-safe functions** and **idiot-proof steps**, to minimise user errors, such as by creating very simple and easy-to-navigate interface.

**Excluded Diagrams**\
The excluded diagrams were excluded because they are able to represent interactions between multiple actors and systems in high detail. However our current project has very limited interactions between actors and focuses more on interactions between user and system, and between parts of the system.

[\[Back to Top\]](#cw1lab-05-report-specifications-document)

## Prototypes
The following prototypes illustrates how the app could look like after development. Take note that various things can change after further discussions with the client.

Click on the picture to view full resolution
<table>
  <tr>
    <th><b>Image Name</b></th>
    <th><b>Prototype Image</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td>01 - Main Interface</td>
    <td>
      <a href="images/01-main-interface-hq.jpg">
        <img src="images/01-main-interface.jpg">
      </a>
    </td>
    <td>
      This is the main interface that the user will see the moment they enter the app.<br><br>
      Users can find most of the functions here on this page and can navigate quickly to the various functions that the app have through clicking the buttons.<br><br>
      They can also view various information in summarized form for easy understanding.
    </td>
  </tr>
  <tr>
    <td>02 - Login Interface</td>
    <td>
      <a href="images/02-pic-login-hq.jpg">
        <img src="images/02-pic-login.jpg">
      </a>
    </td>
    <td>
      This is the interface for the login page. Users can access this by either clicking on the login button on main interface or go to "me".<br>
      (<i>only when not logged in yet</i>)
      <br><br>
      <u>After logging in</u>, they will be automatically redirected to the account info page (3). Now the 'login' button on the main interface will change to <b>'Account' button</b>. The 'me' button will <b>take users to the account info page</b> instead of the login page.
    </td>
  </tr>
  <tr>
    <td>03 - Account Info</td>
    <td>
      <a href="images/03-pic-accinfo-hq.jpg">
        <img src="images/03-pic-accinfo.jpg">
      </a>
    </td>
    <td>
      This is the account information interface. Most functions described in the <a href="../lab-04/lab04report.md">requirements</a> are realised in this page.<br><br>
      The page is divided into <b>four parts</b>:
      <ol>
      <li><b>Balance information</b></li>
      Shows the balance information. Including the total balance in the account, available balance for spending, and unavailable balance.<br>
      (i.e. set for bills/scheduled payments/savings).
      <li><b>Financial information</b></li>
      Shows the income-outflow information. If user press on the 'view details', they will be able to check the income and spending details (Picture 04).
      <li><b>Financial Management Helper</b></li>
      Shows various finance-managing functions. Users are able to <i>DIY</i> their budget plan here.
      <li><b>Extra space (not shown)</b></li>
      When the users scroll down, they will be able to see other things that our client may want to show.<br>
      (e.g. advertisements, investments, etc.)
      <br><br>
      Information visibility are toggleable by pressing the top section.<br><br>
      Timeout may happen if users are inactive for too long. Login will need to be done again when timeout occurs.
    </td>
  </tr>
  <tr>
    <td>04 - Detailed financial information</td>
    <td>
      <a href="images/04-pic-month-info-hq.jpg">
        <img src="images/04-pic-month-info.jpg">
      </a>
    </td>
    <td>
      This page can be reached by pressing 'view details' in the Account info page.
      <br><br>
      Shows <b>detailed transaction information</b> including income and spending and sorted by date and time. Users will be able to do <b>swipe operations</b> on each record to modify category and/or description - <b>but not amount or time</b>.
      <br><br>
      Changing default category for a transaction can also be done through the swipe operations. Simply swipe to edit, and select the default category that you want to associate with that specific merchant. <br>
      (e.g. Unicom as Phone bill/SIM Card Charge)
    </td>
  </tr>
  <tr>
    <td>05 - Budget Planner</td>
    <td>
      <a href="images/05-pic-budget-planner-hq.jpg">
        <img src="images/05-pic-budget-planner.jpg">
      </a>
    </td>
    <td>
      Users can <b>create their transaction categories</b> here and <b>set defaults</b> (not shown). In addition, they can also set the 'expected' spending amount for each category they have created.
    </td>
  </tr>
  <tr>
    <td>06 - Current Plan</td>
    <td>
      <a href="images/06-pic-current-budget-hq.jpg">
        <img src="images/06-pic-current-budget.jpg">
      </a>
    </td>
    <td>
      This page will shows very detailed information about all <b>budget-related info</b>. Users will be able to track spending in each category. In addition, if the user has set a maximum  amount for a category, the app will <b>warn the user</b> if they are reaching/past the set amount.
    </td>
  </tr>
</table>

[\[Back to Top\]](#cw1lab-05-report-specifications-document)

## Context Diagram
![Context Diagram](images/context-diagram.jpg)

### Description
The financial manager app have various functionalities to meet the client's demand. This diagram has summarised the relationship and interactions between the different systems that the app has.

The app will be **integrated with the Bank's customer database** so that all changes done in app will be updated in real-time to the database.

The app will need to **verify account credentials** and thus will check if login information is the same with the credentials that the user has registered with the bank. In addition, since there are **multiple account types**, the app will also **check which type** the current logged-in user is and **display the specific functions** that are **only available to that user-type**.

The **Finance Management App Component** is our main system which contains several subsystems that is responsible for various financial management functions, such as **Budgetting**, **Transaction Record**, **Management**, etc. Details below.
+ **Budget** subsystem
  - Mainly responsible for budget-related functionalities
  - Users interacts with this subsystem when **setting their monthly budget**, **adjusting income info**, **setting savings**, **setting 'set-aside' money**, **setting fixed spending**, etc.
+ **Financial Manager (Management)** subsystem
  - This subsystem mostly runs automatically in response to a user request.
  - Main tasks include **processing request**, **processing queries**, **various budget-related calculation**, **report generation**, etc.
+ **Transaction** subsystem
  - This subsystem is able to **automatically retrieve data** from the Bank's database
  - Mainly **stores information regarding various transactions**.
  - **Outflows will be recorded** as payments and **categorised as set** by the user.
  - It will also **categorise additional incomes** if detected in the account database.
  - If the user does **transfer operations** (receive or send to/from other account), it is also recorded and will be displayed as inflow or outflow accordingly.

[\[Back to Top\]](#cw1lab-05-report-specifications-document)

## Scenarios
The following scenarios were constructed with respect to the use-case identified and relevant personas are used to demonstrate the functionalities described in the particular scenario.\
The persona's name can be clicked to view the profile page, if you want to review their background, information, and account type.

### General Functionalities
<table>
  <tr><th colspan="2"><b>Scenario APP</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers of BoC
      <ol>
        <li>Families</li>
        <li>Single-Account Users</li>
        <li>Multi-Account Users</li>
        <li>Joint-Account Users</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>Help BoC clients to manage their money easier</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>BoC is looking for a replacement to spreadsheet finance-management that most of their customers are doing. They are seeking for an easier and more efficient method for their customers to manage their financial plans.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
    <ol>
      <li>Let users know their money left for fun and casual spending.</li>
      <li>Let users know how much they spend on different categories.</li>
      <li>Let users know their budgets and account information.</li>
      <li>Let users know how much money is exactly fixed for bills.</li>
      <li>Set an 'average' or 'usual cost' for them.</li>
      <li>Create a standard amount that is 'set aside'.</li>
      <li>Calculate an available spare budget.</li>
      <li>Categorise consumption in one shop and be able to change category for a transaction.</li>
    </ol>
    </td>
  </tr>
</table>

### Specific Actions
#### Jane's Case 01: Categorised Spending
<table>
  <tr><th colspan="2"><b>Scenario APP (Categorised Spending)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor01_singleaccount.md">Jane</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>Jane wants to know how her money is spent on different categories.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Jane wants to find out her spending pattern as to maximise her investments and savings.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>Jane opens the app</li>
        <li>Log-in using credential</li>
        <li>View summary in main page of the app (bottom middle of the screen)</li>
        <li>For more detail, she can go to "Me" >> "View Details"</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      If the information is not shown in the main screen, check whether she is logged in. If problem persists, check "settings" and see whether she had enabled the function.
    </td>
  </tr>
</table>

#### Eric's Case 01: Budgeting and Leftover for Casual Spending
<table>
  <tr><th colspan="2"><b>Scenario APP (Budget and Leftovers)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor01_multiaccount.md">Eric</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>Eric wants to see how much money is left in his budget that he can spend for fun and casual spending for his family.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Eric's son is going to turn 5 years old in one week. Eric wants to splurge a bit to celebrate his son's birthday, but he needs to know the amount he can splurge.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>Eric opens the app and login using credential</li>
        <li>Since Eric is a multi-account user, he selects the appropriate account to view</li>
        <li>Eric goes to "budget" through main page</li>
        <li>Checks the budget information and leftover for casual spending</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      The app should notify Eric if he is overspending/passed the set limit for the account/category.
    </td>
  </tr>
</table>

#### John's Case 01: Fixed spending and 'set aside' budget
<table>
  <tr><th colspan="2"><b>Scenario APP (Fixed spending and 'set aside' budget)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor03_jointaccount.md">John</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>John needs to know the amount of money he needs to spend every month.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Since John's is mainly financed through scholarship and his parent's allowance, he needs to maintain his budget really carefully. He wants to see his fixed spending and also set a 'set-aside budget' for every month.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>John or his parents opens the app and login using their respective credentials</li>
        <li>The user accesses the budgeting function from the main page</li>
        <li>If he needs to check other information first, he can go to "Me" before going to the function (also accessible through "Me")</li>
        <li>The user then sets his budget and categories by inputting the expected amount</li>
        <li>The app saves and automatically calculate his budget</li>
        <li>John and his parents can see the result through "Current Budget Plan"</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      If John doesn't have enough money in his account balance to fulfill the "bills", the app should sent a notice to his phone so that John can go to the bank to deposit more money or contact his parents to transfer him money.
    </td>
  </tr>
</table>

#### Eric's Case 02: Default category and changing category for selected transactions
<table>
  <tr><th colspan="2"><b>Scenario APP (Budget and Leftovers)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor01_multiaccount.md">Eric</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>Eric wants to categorise the spending for a cake and other things as 'gifts' instead of their respective default categories.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Eric's son is going to turn 5 years old in one week. Eric knows he has enough money to splurge on his son's birthday and is planning to buy cakes and some other gifts. He needs to categorise these as 'gifts' instead of say, 'food' or 'clothes'.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>Eric did some transactions using various means (e.g. Alipay, Taobao, etc.)</li>
        <li>Eric opens the app and login using credentials</li>
        <li>He clicks on the "spending details" on main page to check his recent spending</li>
        <li>Eric then looks for the spending for cakes, clothing, and toys for his son's birthday gift</li>
        <li>He does swipe operation to modify the spending information</li>
        <li>He can then change the category from whatever they were to 'gifts'</li>
        <li>The app automatically recalculates the affected categories and return with updated information</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      If it's Eric's first time shopping in that particular store, he should be able to set a default category for that store.
    </td>
  </tr>
</table>

### Account-Type Specific Scenarios
#### Eric's Case 03: Multi-Account Management
<table>
  <tr><th colspan="2"><b>Scenario APP (Multi-Account Management)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor01_multiaccount.md">Eric</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>Eric needs to manage multiple accounts regularly for his family.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Eric is responsible for his family's finance. But he finds it difficult to manage multiple accounts using spreadsheet, so he's using the app to manage the accounts. He needs to check the accounts periodically.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>Eric opens the app and login with his credentials</li>
        <li>Since he's registered in Bank of China as having multiple account, the multi-account functionalities should be visible</li>
        <li>Eric can go to the Multiple-Account function and select the account he wants to manage</li>
        <li>After choosing, he can do other typical actions like checking balance or setting budget</li>
        <li>Switching accounts can be done through the multi-account function</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      The multi-account functionalities should not be visible for non-multi-account holders.
    </td>
  </tr>
</table>

#### John's Case 02: Joint-Account User-specific report
<table>
  <tr><th colspan="2"><b>Scenario APP (Joint-Account User-specific report)</b></th></tr>
  <tr>
    <td><b>Title</b></td>
    <td>New app for customers of BoC</td>
  </tr>
  <tr>
    <td><b>Context</b></td>
    <td>BoC want a new app for personal finance management</td>
  </tr>
  <tr>
    <td><b>Actors</b></td>
    <td>
      Customers (<a href="../lab-04/personas/actor03_jointaccount.md">John</a>)
    </td>
  </tr>
  <tr>
    <td><b>Technology</b></td>
    <td>Phone</td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>John's parents want to generate John's spending report to see how he has been spending his money.</td>
  </tr>
  <tr>
    <td><b>Rationale</b></td>
    <td>Since John's is mainly financed through scholarship and his parent's allowance, his parents want to make sure he is spending responsibly and not spending on needless things.</td>
  </tr>
  <tr>
    <td><b>Plots</b></td>
    <td>
      <ol>
        <li>John's parents open the app and login using their credentials</li>
        <li>Since they're registered as a joint-account holder, the joint-account functionalities should be visible</li>
        <li>John's parents can go to the joint-account functions to generate report</li>
        <li>They choose 'John' and generate the report</li>
        <li>The report is generated and can be saved in csv format, if they choose to</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td><b>Notes</b></td>
    <td>
      If the user is not a joint-account holder, the joint-account specific functions should not be visible.
    </td>
  </tr>
</table>

[\[Back to Top\]](#cw1lab-05-report-specifications-document)
