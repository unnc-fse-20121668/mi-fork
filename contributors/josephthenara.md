## Hi there, I'm Joseph!
[Back to README](../README.md)

![Joseph's Picture](images/Joseph_img.jpg "Joseph Then")

### About me
> Being awake is fun, being asleep is bliss~

My name is **Joseph Manuel Thenara** - if it's too long, **Joseph** or **Nara** is okay!\
I often refer to myself as '*a sleepy Indonesian scholar lost in China*' But don't take me too seriously. (except the sleepy part - that one's true!).\
As you might have guessed, I love sleeping. When I'm awake though, I love to read and do stupid random things. Oh, I also do photo and videography.\
***I love cats***

### My - *very* - complete biodata
<details>
  <summary>Click to view</summary>

  | Biodata      | Details                                       |
  |------------- |-----------------------------------------------|
  | Full Name    | Joseph Manuel Thenara                         |
  | Nickname     | Joseph or Nara                                |
  | Chinese Name | 鄧龍欣                                         |
  | Birthdate    | 30 March 2000                                 |
  | Nationality  | Indonesian                                    |
  | Languages    | English - Bahasa Indonesia - Mandarin Chinese |


</details>

### Contact Me!
Instagram: [JosephThen3320](https://www.instagram.com/josephthen3320)\
LinkedIn: [Joseph Manuel Thenara](https://www.linkedin.com/in/josephthenara)\
Email: [Fire me an email!](mailto:scyjt1@nottingham.edu.cn "Send Joseph an email!")\
Website: [Visit my website!](http://www.josephthenara.com/ "Visit Joseph's website!")
