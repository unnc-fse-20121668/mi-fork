import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;

import static java.lang.System.exit;

/** Main entry point into the BoC Financial Manager App. **/
public class BoCApp {
    /** List of all user transactions. **/
    public static ArrayList<BoCTransaction> UserTransactions = new ArrayList<BoCTransaction>(); //initialised to avoid nullpointerexception
    /** List of all user categories. **/
    public static ArrayList<BoCCategory> UserCategories = new ArrayList<BoCCategory>(); //initialised to avoid nullpointerexception

    /**
     * Start the Financial Manager App.
     * @param args not used for arguments
     */
    public static void main(String[] args) {

        // SETUP EXAMPLE DATA //
        UserCategories.add(new BoCCategory("Unknown"));
        BoCCategory BillsCategory = new BoCCategory("Bills");
        BillsCategory.setCategoryBudget(new BigDecimal("120.00"));
        UserCategories.add(BillsCategory);
        BoCCategory Groceries = new BoCCategory("Groceries");
        Groceries.setCategoryBudget(new BigDecimal("75.00"));
        UserCategories.add(Groceries);
        BoCCategory SocialSpending = new BoCCategory("Social");
        SocialSpending.setCategoryBudget(new BigDecimal("100.00"));
        UserCategories.add(SocialSpending);

        UserTransactions.add(new BoCTransaction("Rent", new BigDecimal("850.00"), 0));
        UserTransactions.add(new BoCTransaction("Phone Bill", new BigDecimal("37.99"), 1));
        UserTransactions.add(new BoCTransaction("Electricity Bill", new BigDecimal("75.00"), 1));
        UserTransactions.add(new BoCTransaction("Sainsbury's Checkout", new BigDecimal("23.76"), 2));
        UserTransactions.add(new BoCTransaction("Tesco's Checkout", new BigDecimal("7.24"), 2));
        UserTransactions.add(new BoCTransaction("RockCity Drinks", new BigDecimal("8.50"), 3));
        UserTransactions.add(new BoCTransaction("The Mooch", new BigDecimal("13.99"), 3));

        for (int x = 0; x < UserTransactions.size(); x++) {
            BoCTransaction temp = UserTransactions.get(x);
            int utCat = temp.transactionCategory();
            BoCCategory temp2 = UserCategories.get(utCat);
            temp2.addExpense(temp.transactionValue());
            UserCategories.set(utCat, temp2);
        }

        // MAIN FUNCTION LOOP

        String mainMenuPrompt = "\nWhat do you want to do? \n" +
                "	[O]		Overview\n" +
                "	[T]		List all transactions\n" +
                "	[num] 	Show category [num]\n" +
                "	[C] 	Change transaction category\n" +
                "	[A] 	Add Transaction\n" +
                "	[N] 	Create new category\n" +
                "	[X] 	Exit";

        CategoryOverview();

		/*System.out.println(
				"\nWhat do you want to do?\n T = List All [T]ransactions, [num] = Show Category [num], A = [A]dd Transaction, X = E[x]it");
		*/
        System.out.println(mainMenuPrompt);
        System.out.print(">> Command:  ");

        Scanner in = new Scanner(System.in);
        String title;
        BigDecimal value;

        while (in.hasNextLine()) {
            String s = in.next();
            String option = s.toLowerCase();	// Accepts both upper and lowercase inputs.

            try {
                switch (option) {
                    case "t":
                        ListTransactions();
                        break;

                    case "o":
                        CategoryOverview();
                        break;

                    case "c":
                        ListTransactions(); // for better usability
                        System.out.println("Which transaction ID?");
                        in.nextLine(); // to remove read-in bug
                        int tID = Integer.parseInt(in.nextLine());
                        System.out.println("\t- " + UserTransactions.get(tID - 1).toString());
                        System.out.println("Which category will it move to?");
                        CategoryOverview();
                        int newCtg = Integer.parseInt(in.nextLine());

                        ChangeTransactionCategory(tID - 1, newCtg - 1); // -1 for bridge the gap between displayed index and user input

                        break;

                    case "n":
                        System.out.println("What is the title of the category?");
                        in.nextLine(); // to remove read-in bug
                        title = in.nextLine();

                        System.out.println("What is the budget for this category?");
                        value = new BigDecimal(in.nextLine());

                        AddCategory(title, value);
                        System.out.println("[Category added]");
                        CategoryOverview();

                        break;

                    case "a":
                        System.out.println("What is the title of the transaction?");
                        in.nextLine(); // to remove read-in bug
                        title = in.nextLine();

                        System.out.println("What is the value of the transaction?");
                        value = new BigDecimal(in.nextLine());

                        CategoryOverview();
                        System.out.println("What is the category of the transaction?");
                        int ctg = Integer.parseInt(in.nextLine());

                        if (ctg>0 && ctg<=UserCategories.size()) {  //check if chosen category exists
                            AddTransaction(title, value, ctg-1);
                            System.out.println("[Transaction added]");
                        }
                        else
                            System.out.println("Command not recognised, please try again.");

                        break;

                    case "x":
                        System.out.println("Goodbye!");
                        exit(0);

                    default:
                        if (Integer.parseInt(option) > 0) {
                            int num = Integer.parseInt(option);
                            ListTransactionsForCategory(num - 1); //to bridge the gap between displayed index and user input

                        } else {
                            System.out.println("Command not recognised, please try again.");
                        }
                }
            } catch (Exception e) {
                System.out.println("Command not recognised, please try again.");
                // System.out.println("Something went wrong: " + e.toString() + "\n");
            }

            System.out.println(mainMenuPrompt);
            System.out.print(">> Command: ");

        }
        in.close();
    }

    /** Reads all of user transactions and list them out in the format: "X) TransactionName - $ XXX.XX" **/
    public static void ListTransactions() {
        for (int x = 0; x < UserTransactions.size(); x++) {
            BoCTransaction temp = UserTransactions.get(x);
            System.out.println((x + 1) + ") " + temp.toString());
        }
    }

    /** Creates summary for each category and print out a list in the format " */
    public static void CategoryOverview() {
        for (int x = 0; x < UserCategories.size(); x++) {
            BoCCategory temp = UserCategories.get(x);
            System.out.println((x + 1) + ") " + temp.toString());
        }

    }

    /**
     * Lists all transactions for specified category index.
     *
     * @param chosenCategory The index of the category to view transactions for.
     */
    public static void ListTransactionsForCategory(int chosenCategory) {
        for (int x = 0; x < UserTransactions.size(); x++) {
            BoCTransaction temp = UserTransactions.get(x);
            if (temp.transactionCategory() == chosenCategory) {
                System.out.println((x + 1) + ") " + temp.toString());
            }
        }
    }

    // Removed user interaction in Three methods below in order to clarify what should be tested

    /**
     * Adds a new transaction record.
     *
     * @param title     The name of the transaction.
     * @param value     The amount of the transaction.
     * @param ctg       The transaction category index.
     */
    private static void AddTransaction(String title, BigDecimal value, int ctg) {

        BoCTransaction newT = new BoCTransaction();

        try {
            newT.setTransactionName(title);
            newT.setTransactionValue(value);
            newT.setTransactionCategory(ctg);
            newT.setTransactionTime(new Date());
        } catch (Exception e) {
            System.out.println(e);
        }
        UserTransactions.add(newT);
        BoCCategory itsCtg = UserCategories.get(newT.transactionCategory()); //update spent on the chosen category
        itsCtg.addExpense(newT.transactionValue());
    }

    /**
     * Modifies the category of a specified transaction record.
     *
     * @param tID       The unique ID of a target transaction record.
     * @param newCtg    The new category for which the target record should be re-assigned to.
     */
    private static void ChangeTransactionCategory(int tID, int newCtg) {

        BoCTransaction tempT    = UserTransactions.get(tID);
        BoCCategory tempC       = UserCategories.get(tempT.transactionCategory());

        tempC.removeExpense(tempT.transactionValue());
        tempT.setTransactionCategory(newCtg);

        UserTransactions.set(tID, tempT);

        tempC = UserCategories.get(newCtg);
        tempC.addExpense(tempT.transactionValue());

        UserCategories.set(newCtg, tempC);
    }

    /**
     * Adds a new transaction category and allocates an index.
     *
     * @param title     The name of the new category.
     * @param budget    The budget for this category.
     */
    private static void AddCategory(String title, BigDecimal budget) {
        BoCCategory newC = new BoCCategory();

        try {
            newC.setCategoryName(title);
            newC.setCategoryBudget(budget);
        } catch (Exception e) {
            System.out.println(e);
        }

        UserCategories.add(newC);
        System.out.println(newC);
    }

    /**
     * Not used.
     */
    public BoCApp() {
        // Not used
    }

}
