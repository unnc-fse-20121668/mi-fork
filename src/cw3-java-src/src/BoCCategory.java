import java.math.BigDecimal;

/** [Sub-System] Transaction category management. **/
public class BoCCategory {
    private String CategoryName;
    private BigDecimal CategoryBudget;
    private BigDecimal CategorySpend;

    /** Creates a new transaction category using default values. */
    public BoCCategory() {
        CategoryName = "New Category";
        CategoryBudget = new BigDecimal("0.00");
        CategorySpend = new BigDecimal("0.00");
    }

    /**
     *  Creates a new transaction category.
     *
     * @param newTitle      The name of the new category of type String - will be set to CategoryName.
     */
    public BoCCategory(String newTitle) {
        CategoryName = newTitle;
        CategoryBudget = new BigDecimal("0.00");
        CategorySpend = new BigDecimal("0.00");
    }

    /**
     * Getter for category name.
     *
     * @return  name of category.
     */
    public String CategoryName() {
        return CategoryName;
    }

    /**
     * Getter for category budget.
     *
     * @return  budget value of a category.
     */
    public BigDecimal CategoryBudget() {
        return CategoryBudget;
    }

    /**
     * Getter for spending amount of a category.
     *
     * @return  value of spending in a category.
     */
    public BigDecimal CategorySpend() {
        return CategorySpend;
    }

    /**
     * Sets new name for current category.
     *
     * @param newName   The new name of the category of type String - sets to CategoryName.
     */
    public void setCategoryName(String newName) {
        CategoryName = newName;
    }

    /**
     * Sets new budget value for current category.
     *
     * @param newValue  The new budget value for the category of type BigDecimal - sets to CategoryBudget.
     */
    public void setCategoryBudget(BigDecimal newValue) {
        // 1 means bigger, -1 means smaller, 0 means same
        if (newValue.compareTo(new BigDecimal("0.00")) == 1) {
            CategoryBudget = newValue;
        }
    }

    /**
     * Adds the value of a transaction to the spending value of that transaction's category.
     *
     * @param valueToAdd    The value of a transaction to be added to category's spending.
     */
    public void addExpense(BigDecimal valueToAdd) {
        if (valueToAdd.compareTo(new BigDecimal("0.00")) > 0) {         // added check to disallow invalid value
            CategorySpend = CategorySpend.add(valueToAdd);
        }
    }

    /**
     * Removes the value of a transaction from the spending value of that transaction's category.
     *
     * @param valueToRemove The value of a transaction to be removed from category's spending.
     */
    public void removeExpense(BigDecimal valueToRemove) {
        if (valueToRemove.compareTo(new BigDecimal("0.00")) > 0) {      // added check to disallow invalid value
            CategorySpend = CategorySpend.subtract(valueToRemove);
        }
    }

    /** Resets the spending value for current category to zero */
    public void resetBudgetSpend() {
        CategorySpend = new BigDecimal("0.00");
    }

    /**
     * Getter for the remaining budget of a category.
     *
     * @return  budget remaining value for a category.
     */
    public BigDecimal getRemainingBudget() {
        BigDecimal remainingBudget = CategoryBudget.subtract(CategorySpend);
        return remainingBudget;
    }

    /**
     * Overrides the normal return.
     * When this class is called as a string, this function
     * will formats the return value to show
     * the category name, budget, spending, and status.
     *
     * @return  Category record in the form of: "Name (budget) - spending (value and status)"
     */
    @Override
    public String toString() {
		/*
		return CategoryName + "(¥" + CategoryBudget.toPlainString() + ") - Est. ¥" + CategorySpend.toPlainString()
				+ " (¥" + getRemainingBudget().toPlainString() + " Remaining)";
		 */
        String budgetRemaining = calcRemainingStatus();
        String result = CategoryName + " (Budget: ¥" + CategoryBudget.toPlainString() + ") - ¥"
                + CategorySpend.toPlainString() + budgetRemaining;

        return result;
    }

    /**
     * Calculates the remaining budget
     * and set budget status to either 'overspent' or 'remaining'.
     *
     * @return  A formatted string of the remainder value and the text 'overspent' or 'remaining'
     *
     * Author: Joseph Thenara
     */
    String calcRemainingStatus() {
        BigDecimal budgetRemaining = null;
        String budgetStatus, strReturn = "";

        if (CategoryBudget.compareTo(CategorySpend) >= 0) {     // Checks if Budget >= Spend
            budgetStatus    = "Remaining";
            budgetRemaining = CategoryBudget.subtract(CategorySpend);
            strReturn = " (¥" + budgetRemaining + " " + budgetStatus + ")";
        } else {        // When Budget < Spending
            budgetStatus    = "Overspent";
            budgetRemaining = CategorySpend.subtract(CategoryBudget);
            strReturn = " (¥" + budgetRemaining + " " + budgetStatus + ")";
        }

        return strReturn;

    }
}
