import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;


class BoCAppTest {

    static BoCApp appclass;
    static BoCCategory ctgclass;
    static BoCTransaction txnclass;
    static int lastIndex;
    static String cTitle;

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeAll
    public static void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @BeforeAll
    static void setup() {       // Test variable initialisation

        appclass = new BoCApp();
        ctgclass = new BoCCategory();
        txnclass = new BoCTransaction();

        lastIndex = 0;
    }

    @BeforeAll static void setupLists() {   // Initialise lists by populating with test data
        // BoCApp.UserCategories = new ArrayList<BoCCategory>();
        BoCApp.UserCategories.add(new BoCCategory("Unknown"));
        BoCCategory BillsCategory = new BoCCategory("Bills");
        BillsCategory.setCategoryBudget(new BigDecimal("120.00"));
        BoCApp.UserCategories.add(BillsCategory);
        BoCCategory Groceries = new BoCCategory("Groceries");
        Groceries.setCategoryBudget(new BigDecimal("75.00"));
        BoCApp.UserCategories.add(Groceries);
        BoCCategory SocialSpending = new BoCCategory("Social");
        SocialSpending.setCategoryBudget(new BigDecimal("100.00"));
        BoCApp.UserCategories.add(SocialSpending);

        // BoCApp.UserTransactions = new ArrayList<BoCTransaction>();
        BoCApp.UserTransactions.add(new BoCTransaction("Rent", new BigDecimal("850.00"), 0));
        BoCApp.UserTransactions.add(new BoCTransaction("Phone Bill", new BigDecimal("37.99"), 1));
        BoCApp.UserTransactions.add(new BoCTransaction("Electricity Bill", new BigDecimal("75.00"), 1));
        BoCApp.UserTransactions.add(new BoCTransaction("Sainsbury's Checkout", new BigDecimal("23.76"), 2));
        BoCApp.UserTransactions.add(new BoCTransaction("Tesco's Checkout", new BigDecimal("7.24"), 2));
        BoCApp.UserTransactions.add(new BoCTransaction("RockCity Drinks", new BigDecimal("8.50"), 3));
        BoCApp.UserTransactions.add(new BoCTransaction("The Mooch", new BigDecimal("13.99"), 3));

        for (int x = 0; x < BoCApp.UserTransactions.size(); x++) {
            BoCTransaction temp = BoCApp.UserTransactions.get(x);
            int utCat = temp.transactionCategory();
            BoCCategory temp2 = BoCApp.UserCategories.get(utCat);
            temp2.addExpense(temp.transactionValue());
            BoCApp.UserCategories.set(utCat, temp2);
        }
    }

    @BeforeEach void resetOutStream() { outContent.reset(); }

    @Test // check overview to see remaining budget
    void testUseCase1() {

        // setup expected output
        StringBuilder expect = new StringBuilder();

        for (int x = 0; x<BoCApp.UserCategories.size(); x++) {
            ctgclass = BoCApp.UserCategories.get(x);
            expect.append(((x + 1) + ") " + ctgclass.toString() + "\n"));
        }

        // compare with the actual output
        BoCApp.CategoryOverview();
        assertEquals(expect.toString().trim(), outContent.toString().trim().replace("\r",""));
    }

    @Test  // testing to create new category
    void testUseCase2() throws IllegalAccessException, IllegalArgumentException,
                               InvocationTargetException, NoSuchMethodException, SecurityException{

        BoCCategory expected, actual;

        // setup expected output
        cTitle = "Daily spending";
        BigDecimal cBudget = new BigDecimal(750.00);
        expected = new BoCCategory(cTitle);
        expected.setCategoryBudget(cBudget);

        //access and invoke AddCategory()
        Method m = BoCApp.class.getDeclaredMethod("AddCategory", String.class, BigDecimal.class);
        m.setAccessible(true);
        m.invoke(appclass, cTitle, cBudget);

        // check if the created category added to the list successfully
        lastIndex = (BoCApp.UserCategories.size()) - 1;
        actual = BoCApp.UserCategories.get(lastIndex);
        assertEquals(expected.toString().trim(),actual.toString().trim());
    }

    @Test  // check transactions by category
    void testUseCase3() {

        BoCTransaction TFS;
        int chosenCtg = 4;

        // setup expected output
        StringBuilder sb = new StringBuilder();

        for (int x = 0; x<BoCApp.UserTransactions.size(); x++) {

            TFS = BoCApp.UserTransactions.get(x);
            if (chosenCtg == TFS.transactionCategory()) {
                sb.append(((x + 1) + ") " + TFS.toString() + "\n"));
            }
        }
        byte[] expect = sb.toString().getBytes();

        // compare with the actual output
        BoCApp.ListTransactionsForCategory(chosenCtg);
        byte[] actual= outContent.toByteArray();
        assertArrayEquals(expect, actual);
    }

    @Test  // create "Rent" category, then move and add one transaction to it
    void testUseCase4() throws IllegalAccessException, IllegalArgumentException,
                               InvocationTargetException, NoSuchMethodException, SecurityException{

        /* 1.create category "Rent" */

        BoCCategory expected, actual;
        cTitle = "Rent";
        BigDecimal cBudget = new BigDecimal(2600.00);

        // setup expected output
        expected = new BoCCategory(cTitle);
        expected.setCategoryBudget(cBudget);

        // access and invoke AddCategory()
        Method m1 = BoCApp.class.getDeclaredMethod("AddCategory", String.class, BigDecimal.class);
        m1.setAccessible(true);
        m1.invoke(appclass, cTitle, cBudget);

        // check if the created category added to the list successfully
        lastIndex = (BoCApp.UserCategories.size()) - 1;
        actual = BoCApp.UserCategories.get(lastIndex);
        assertEquals(expected.toString().trim(),actual.toString().trim());


        /* 2. move "Rent" transaction from category "Unknown" to "Rent" */

        int tID, newCtgID;
        tID = 0;
        newCtgID = 4;

        BoCTransaction txn = BoCApp.UserTransactions.get(tID);
        BoCCategory oldCtg = BoCApp.UserCategories.get(txn.transactionCategory());
        // setup expected output
        BigDecimal expected1 = new BigDecimal("0.00"); // 850 - 850
        BigDecimal expected2 = new BigDecimal("850.00"); // 0 + 850
        //access and invoke ChangeTransactionCategory()
        Method m2 = BoCApp.class.getDeclaredMethod("ChangeTransactionCategory", int.class, int.class);
        m2.setAccessible(true);
        m2.invoke(appclass, tID , newCtgID);

        BoCCategory newCatg = BoCApp.UserCategories.get(txn.transactionCategory());

        assertAll("change category",

                // check if transaction's category has been changed
                () -> assertEquals(newCtgID, txn.transactionCategory()),
                // check if "Unknown" and "Rent" categories' spent has been updated
                () -> assertEquals(expected1, oldCtg.CategorySpend()),
                () -> assertEquals(expected2, newCatg.CategorySpend())
        );

        /* 3. add "Office rent" transaction to "Rent" category */

        String tTitle = "Office rent";
        BoCTransaction expected3, actual3;
        BigDecimal tValue = new BigDecimal(1250.99);

        // setup expected output
        expected3 = new BoCTransaction(tTitle, tValue, newCtgID);

        // access and invoke AddTransaction()
        Method m = BoCApp.class.getDeclaredMethod("AddTransaction", String.class, BigDecimal.class, int.class);
        m.setAccessible(true);
        m.invoke(appclass, tTitle, tValue, newCtgID);

        // check if the created category added to the list successfully
        lastIndex = (BoCApp.UserTransactions.size()) - 1;
        actual3 = BoCApp.UserTransactions.get(lastIndex);
        assertEquals(expected3.toString(),actual3.toString());
    }

    @AfterAll
    public static void cleanUpStreams() {
        System.setOut(null);
    }
}
