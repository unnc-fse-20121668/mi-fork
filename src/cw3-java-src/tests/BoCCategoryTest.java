import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class BoCCategoryTest {

    static String name1, name2, name3, name4, name5;
    static BigDecimal num1, num2, num3, num4, num5, num6, num7, temp, zero;
    static String str, strExpect, strEmpty;

    @BeforeAll
    static void setup() {   // Test variables initialisation
        name1 = "Test";
        name2 = "";
        name3 = "Alternate";
        name4 = "3320 Category";
        name5 = "!@#$%^&*()_-+=";

        zero  = new BigDecimal("0.00");
        num1  = new BigDecimal("1000.00");
        num2  = new BigDecimal("322.81");
        num3  = new BigDecimal("10.00");
        num4  = new BigDecimal("-100.00");
        num5  = new BigDecimal("1.00");
        num6  = new BigDecimal("11.00");
        num7  = new BigDecimal("1200");

        str = "New Category (Budget: ¥0.00) - ¥0.00 (¥0.00 Remaining)";
        strExpect = "New Category";
        strEmpty = null;
    }

    @Ignore
    void test() {   // Default template for JUnit Tests --ignored
        fail("Not yet implemented");
    }

/* Author: Qianjun Ma | STARTS */
    @Test
    void testDefaultConstructor() {     // Test for default constructor, when BoCCategory is called without params.
        BoCCategory testDC;
        testDC = new BoCCategory();

        assertEquals("New Category", testDC.CategoryName());
        assertEquals(zero, testDC.CategoryBudget());
        assertEquals(zero, testDC.CategorySpend());

    }


    @Test
    void testMainConstructors() {       // Test for main constructor, when BoCCategory is called with proper params.
        BoCCategory mcNormal, mcAlphanum, mcNullParam, mcSymbol;
        mcNormal    = new BoCCategory(name1);
        mcAlphanum  = new BoCCategory(name4);
        mcSymbol    = new BoCCategory(name5);
        mcNullParam = new BoCCategory(null);

        assertEquals(name1, mcNormal.CategoryName());
        assertEquals(zero, mcNormal.CategoryBudget());
        assertEquals(zero, mcNormal.CategorySpend());

        assertEquals("3320 Category", mcAlphanum.CategoryName());
        assertEquals(zero, mcAlphanum.CategoryBudget());
        assertEquals(zero, mcAlphanum.CategorySpend());

        assertEquals("!@#$%^&*()_-+=", mcSymbol.CategoryName());
        assertEquals(zero, mcSymbol.CategoryBudget());
        assertEquals(zero, mcSymbol.CategorySpend());

        assertNull(mcNullParam.CategoryName());
        assertEquals(zero, mcNullParam.CategoryBudget());
        assertEquals(zero, mcNullParam.CategorySpend());

    }
/* Author: Qianjun Ma | ENDS */


/* Author: Joseph Thenara | STARTS */
    @Test
    void testGetCategoryName() {        // Test if getCategoryName will return correct names as string.
        BoCCategory gcnFirst, gcnSecond, gcnThird, gcnNull;
        name3 = name5 + name4;  // "!@#$%^&*()_-+=3320 Category" Mix of Alphanumeric and symbols

        gcnFirst    = new BoCCategory(name1);
        gcnSecond   = new BoCCategory(name3);
        gcnThird    = new BoCCategory(name2);
        gcnNull     = new BoCCategory(null);

        assertEquals(name1, gcnFirst.CategoryName());
        assertEquals(name3, gcnSecond.CategoryName());

        assertEquals(name2, gcnThird.CategoryName());
        assertNull(gcnNull.CategoryName());
    }

    @Test
    void testGetCategoryBudget() {      // Test if getCategoryBudget will return correct value.
        BoCCategory GCB = new BoCCategory();
        assertEquals(zero, GCB.CategoryBudget());
    }

    @Test
    void testGetCategorySpend() {       // Test if getCategorySpend will return correct value.
        BoCCategory GCS = new BoCCategory();
        assertEquals(zero, GCS.CategorySpend());
    }
/* Author: Joseph Thenara | ENDS */

/* Author: Jialin Li | STARTS */
    @Test
    void testSetCategoryName() {        // Test if setCategoryName will successfully change the category name multiple times.
        BoCCategory SCN = new BoCCategory();
        assertEquals(strExpect, SCN.CategoryName());

        SCN.setCategoryName(name3); // changed category name into "Alternate"
        assertEquals(name3, SCN.CategoryName());

        SCN.setCategoryName(name2); // changed category name into "" (empty string)
        assertEquals(name2,SCN.CategoryName());
    }


    @Test
    void testSetCategoryBudget() {      // Test if setCategoryBudget is able to change budget multiple times.
        BoCCategory SCB = new BoCCategory();
        assertEquals(zero, SCB.CategoryBudget());

        SCB.setCategoryBudget(num3);    // chanced to 10.00
        assertEquals(num3, SCB.CategoryBudget());

        SCB.setCategoryBudget(num4);    // changed to -100.00
        assertNotEquals(num4, SCB.CategoryBudget());    // not equals because negative value is invalid.
    }


    @Test
    void testAddExpense() {             // Test if addExpense is able to add spending for particular category
        BoCCategory AE = new BoCCategory();
        assertEquals(zero, AE.CategorySpend());     // Default value is 0.00

        AE.addExpense(num4);
        assertNotEquals(num4, AE.CategorySpend());  // not equals because negative value is invalid.

        AE.addExpense(num6);
        assertEquals(num6, AE.CategorySpend());     // new expense now = 0 + num6 = num6

    }


    @Test
    void testRemoveExpense() {          // Test if removeExpense is able to remove spending correctly
        BoCCategory RE = new BoCCategory();
        RE.addExpense(num3);    // Initialised with 10.00

        RE.removeExpense(num5); // Removed 1.00
        assertEquals(num3.subtract(num5), RE.CategorySpend());

        RE.removeExpense(num4); // Further removed -100.00
        assertNotEquals(num3.subtract(num5).subtract(num4), RE.CategorySpend());    // not equals because negative value is invalid
        //assertEquals(num3.subtract(num5), RE.CategorySpend());
    }


    @Test
    void testResetSpendTotal() {
        BoCCategory RST = new BoCCategory();
        RST.resetBudgetSpend();
        assertEquals(zero, RST.CategoryBudget());
    }
/* Author: Jialin Li | ENDS */

/* Author: Qianjun Ma | STARTS */
    @Test
    void testCalculateRemainingBudget() {
        BoCCategory CRB = new BoCCategory();
        CRB.setCategoryBudget(num3);

        CRB.addExpense(num5);
        temp = CRB.getRemainingBudget();
        assertEquals(new BigDecimal("9.00"), temp);

        CRB.addExpense(num3);
        temp = CRB.getRemainingBudget();
        assertEquals(new BigDecimal("-1.00"), temp);
    }


    @Test
    void testToString() {
        BoCCategory TS = new BoCCategory();
        assertEquals(str, TS.toString());
    }
/* Author: Qianjun Ma | ENDS */

/* Author: Joseph Thenara | STARTS */

    @Test
    void testCalcRemainingStatus() {
        String crbExpected, crbActual;

        BoCCategory CRB = new BoCCategory();
        CRB.setCategoryBudget(num1);
        CRB.addExpense(num2);

        crbExpected = " (¥"+ num1.subtract(num2) + " Remaining)";
        crbActual   = CRB.calcRemainingStatus();
        assertEquals(crbExpected, crbActual);

        CRB.addExpense(num7);
        crbExpected = " (¥"+ num7.subtract(num1.subtract(num2)) + " Overspent)";
        crbActual   = CRB.calcRemainingStatus();

        assertEquals(crbExpected, crbActual);
    }
/* Author: Joseph Thenara | ENDS */

}
