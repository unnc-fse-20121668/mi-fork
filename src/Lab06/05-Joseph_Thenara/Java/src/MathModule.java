
public class MathModule {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/** Try for myMultLong
		try {
			int test = myMultLong(-2000000000, 6);
			System.out.println(test);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		**/
		
		try {
			double test = myDiv(7, 3);
			System.out.println(test);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
	}

	public static int myMult (int firstNum, int secondNum) {
		
		return firstNum * secondNum;
	}
	
	public static int myMultLong (int x, int y) throws Exception {
		
		long newAns = (long)x * y;
		
		if (newAns > Integer.MAX_VALUE) {
			throw new Exception ("Number too big");
		}
		
		if (newAns < Integer.MIN_VALUE) {
			throw new Exception ("Number too small");
		}
		
		return x * y;
	}

	public static double myDiv(double x, double y) throws Exception {
				
		if (y == 0) {
			throw new IllegalArgumentException("Division by zero.");
		}
		
		return x/y;
	}

}

