import static org.junit.jupiter.api.Assertions.*;

import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class testMathModule {

	private static int input1, input2, input3, input4, input5, zero;	
	
	@BeforeAll
	static void setup() {
		input1 = 3;
		input2 = 6;
		input3 = 2000000000;
		input4 = -2000000000;
		input5 = 7;
		zero = 0;
	}
	
	@Ignore
	void test() {
		fail("Not yet implemented");
	}
	
	@Test
	void test1() {			// Should run
		int myAns = MathModule.myMult(4, 2);
		assertEquals(8, myAns);
	}
	
	@Test
	void test2() {			// Should run				
		int myAns = MathModule.myMult(input1, input2);
		assertEquals(18, myAns);
	}

	@Test
	void test3() {			// Should fail: value out of bound
		int myAns = MathModule.myMult(input2, input3);
		assertEquals(12000000000, myAns);
	}
	
	@Test
	void test4() {			// Should run
		int myAns = 0;
		try {
			myAns = MathModule.myMultLong(input1, input2);
		} catch (Exception e) {
			// Exception
		}
		
		assertEquals(18, myAns);
	}
	
	@Test
	void testError() {
		/**
		Assertions.assertThrows(Exception.class,  ()-> {
			int myAns = MathModule.myMultLong(input2, input3);
		});
		**/
		
		int myAns = 0;
		try {
			myAns = MathModule.myMultLong(input2, input4);
		} catch (Exception e) {
			if (e.getClass() == Exception.class) {
				return; // it passed
			}
		}
		fail ("It failed.");
	}
	
	// Test for myDiv
	@Test
	void testDivision() {
		// (a) assertEquals test for 6 dived by 3
		try {
			assertEquals(2, MathModule.myDiv(input2, input1), 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// (b) assertEquals values between integers
		try {
			assertEquals(2.333, MathModule.myDiv(input5, input1), 0.001);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// (c) Division by zero
	@Test
	void testDivByZero() {
		Assertions.assertThrows(IllegalArgumentException.class, () ->
		MathModule.myDiv(input2, zero));
	}
}
