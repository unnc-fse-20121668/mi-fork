package Math;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestMyMathModule {

    private static int input1, input2, input3, input4;

    @BeforeAll
    static void setup() {
        input1 =  3;
        input2 =  6;
        input3 =  2000000000;
        input4 = -2000000000;
    }

    //test for multiply: pass
    @Test
    void test1() {
        int myAnswer = 0;
        try {
            myAnswer = MathModule.myMultiply(input1, input2);
        } catch(Exception e) { }
        assertEquals(18, myAnswer);
    }

    //test for multiply: fail
    @Test
    void test1Error() {
        int myAnswer = 0;
        try {
            myAnswer = MathModule.myMultiply(input2, input3);
        } catch(Exception e) {
            if(e.getClass() == Exception.class) {
                return; // it passed
            }
        }
        fail("it failed");
    }

    //test for multiply: pass
    @Test
    void test2() {
        int myAnswer = 0;
        try {
            myAnswer = MathModule.myMultiply(input1, input4);
        } catch(Exception e) { }
        assertEquals(18, myAnswer);
    }

    //test for division (float): pass
    @Test
    void test3() {
        double myAnswer = 0;
        try {
            myAnswer = MathModule.myDivide(7, input1);
        } catch(Exception e) { }
        assertEquals(2.3333333333333335, myAnswer);
    }

    //test for division (divide by zero): pass
    @Test
    void test4() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> MathModule.myDivide(input1, 0));

        assertEquals("Divide by zero", thrown.getMessage());
    }

    @Ignore
    void main() {
    }

    @Ignore
    void myMultiply() {
    }

    @Ignore
    void myDivide() {
    }
}