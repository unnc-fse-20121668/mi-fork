package Math;

public class MathModule {

    public static void main(String[] args) {

        try {
            int test = myMultiply(2000000000, 6);
            System.out.println(test);
        } catch(Exception e) {
            System.out.println(e.toString());
        }
    }


    public static int myMultiply(int firstNum, int secondNum) {
        long newAnswer = (long)firstNum * secondNum;
        if (newAnswer > Integer.MAX_VALUE || newAnswer < Integer.MIN_VALUE) {
            try {
                throw new Exception("Number not in range");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return firstNum * secondNum;
    }


    public static double myDivide(int firstNum, int secondNum) {

        if (secondNum == 0.0) {
            throw new IllegalArgumentException("Divide by zero");
        }
        return firstNum / secondNum;
    }
}
